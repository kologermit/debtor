CREATE TABLE `data` (
	`applicant` MEDIUMTEXT,
	`debtor`	MEDIUMTEXT,
	`debt_rub` 	DOUBLE,
	`debt_pen`	DOUBLE,
	`definition_number` TINYTEXT,
	`date1` 	TINYTEXT,
	`more_information` MEDIUMTEXT,	
	`date2`		TINYTEXT,	
	`name_org`	TINYTEXT,
	`files`		LONGTEXT,
	`id`		MEDIUMINT AUTO_INCREMENT,
	PRIMARY KEY (`id`)
);