﻿res = null;
const CL = console.log;
$.ajaxTransport
(
	'json',
	function (options, originalOptions, jqXHR)	{
		if (originalOptions.url == "name_org")		{
			return {
				send: function (headers, completeCallback){
					query = originalOptions.data.q;
					True_res = [];
					False_res = [];
					res = {results: [], status: "OK"};
					data = select_debtor.select2('data')[0];
					CL(data);
					if(data){
						data = data.data.Debtor.LegalCaseList.LegalCaseInfo;
						for(i = 0; i < data.length; i++){
							data[i].Court = data[i].Court.replace(/ый/gi, "ым").replace(/судом/gi, "суд").replace(/суд/gi, "судом");
							if(data[i].Court.search(query))
								True_res.push(data[i].Court);
							else
								False_res.push(data[i].Court);
						}
					}
					for(i = 0; i < True_res.length; i++)
						res.results.push({id: res.results.length, text: True_res[i]});
					for(i = 0; i < False_res.length; i++)
						res.results.push({id: res.results.length, text: False_res[i]});
					if(query){
						res.results.push({id: res.results.length, text: query});
					}
					True_res = null;
					False_res = null;
					query = null;
					data = null;
					CL(res);
					completeCallback(200, 'success', {text: JSON.stringify(res)});
					res = null;
				},
				abort: function(){}
			};
		}
	}
);


const formState = function(state){
	if(state.inn){
		state.text = state.text.replace(/</gi, "&lt").replace(/>/gi, "&gt");
		return $("<table><tbody><tr><td colspan=4>" 
			+ state.text + "</td></tr><tr><td><font color='grey'>ИНН: </font></td><td>" 
			+ state.inn + "</td><td><font color='grey'>ОГРН: </font></td><td>" 
			+ state.ogrn + "</td></tr></tbody></table>");
	}
	else
		return "Поиск...";
}

var safe_value = function(r, fname){return !r[fname] ? '' : r[fname];}

var formatDebtorSelect2ResultItem= function(item){
	var tpl_text = "<table>";
	tpl_text += "<tr><td colspan='6'>" + item.text + "</td></tr>";
	if (item && null !=item && item.data && null !=item.data)
	{
		var data = item.data;
		if (data.Debtor && null !=data.Debtor)		{
			var debtor = data.Debtor;
			tpl_text += "<tr>";
			tpl_text += "<td>ИНН:</td><td>" + safe_value(debtor,'!INN') + "</td>";
			tpl_text += "<td>ОГРН:</td><td>" + safe_value(debtor,'!OGRN') + "</td>";
			tpl_text += "<td>СНИЛС:</td><td>" + safe_value(debtor,'!SNILS') + "</td>";
			tpl_text += "</tr>";
		}
		if (data.Manager && null != data.Manager)		{
			var manager = data.Manager;
			tpl_text += "<tr>";
			tpl_text += "<td>АУ:</td><td colspan='5'>" 
				+ safe_value(manager,'!LastName') + ' '
				+ safe_value(manager,'!FirstName') + ' '
				+ safe_value(manager,'!MiddleName')
			+ "</td>";
			tpl_text += "</tr>";
		}
	}
	tpl_text += "</table>";
	return $(tpl_text);
}

const 
select_applicant = $("select#applicant"),
select_debtor = $("select#debtor"),
select_name_org = $("select#name_org");

var select2_debtor_applicant_width = 575;

function prepare_select_applicant(){
	select_applicant.select2({
		width: select2_debtor_applicant_width,
		placeholder: 'Заявитель (можно ИНН или ОГРН)',
		ajax: {
			url: "applicant.php",
			dataType: "json",
			method: "POST"
		},
		minimumInputLength: 1,
		templateResult: formState
	});
	select_applicant.on('select2:select', on_select_applicant);
}

function prepare_select_debtor(){
	console.log('prepare_select_debtor');
	select_debtor.select2
	({
		width: select2_debtor_applicant_width,
		placeholder: 'Должник (можно ИНН или ОГРН или СНИЛС)',
		ajax: {
			url: "https://probili.ru/efrsb/GetDebtorsForSelect2.php",
			dataType: "json",
		},
		minimumInputLength: 1,
		templateResult: formatDebtorSelect2ResultItem
	});
	select_debtor.on('select2:select', on_select_debtor);
}

select_name_org.select2({
	width: 430,
	placeholder: "Введите имя органиции",
	ajax: {
		url: "name_org",
		dataType: "json",
		success: function(data){
			CL(data);
		},
		error: function(){
			CL("name_org error")
		}
	},
	templateResult: function(data)
	{
		return data.text;
	},
	data: function(){
		return [{id: 1, text: "123"}, {id: 2, text: "234"}]
	}
})

const
button_applicant = $("button#applicant"),
button_debtor = $("button#debtor"),
DW_applicant = $("div#dialog_window_applicant"),
DW_debtor = $("div#dialog_window_debtor"),
date1 = $("#date1"),
date2 = $("#date2"),
applicant_save = $("div#dialog_window_applicant button#save"),
debtor_save = $("div#dialog_window_debtor button#save"),
applicant_cancel = $("div#dialog_window_applicant button#cancel"),
debtor_cancel = $("div#dialog_window_debtor button#cancel")
;
applicant_is_opened = false
debtor_is_opened = false

const on_select_applicant = function(){
	if(select_applicant.select2('data')[0] != undefined && select_applicant.select2('data')[0] != applicant_last)	{
		data = select_applicant.select2('data')[0];
		u = {full: data.address};
		u.index = u.full ? u.full.split(",")[0] : "";
		u.region = u.full.split(",")[1];
		u.city = u.full.split(",")[2];
		r = {full: data.addressf};
		r.index = r.full.split(",")[0];
		r.region = r.full.split(",")[1];
		r.city = r.full.split(",")[2];
		$("div#dialog_window_applicant input#name_org").val(data.title ? data.title : "");
		$("div#dialog_window_applicant input#inn").val(data.inn ? data.inn : "");
		$("div#dialog_window_applicant input#ogrn").val(data.ogrn ? data.ogrn : "");
		$("div#dialog_window_applicant input#uregion").val(u.full ? u.region : "");
		$("div#dialog_window_applicant input#ucity").val(u.city);
		$("div#dialog_window_applicant input#uindex").val(u.index);
		$("div#dialog_window_applicant input#uaddress").val(u.full);
		$("div#dialog_window_applicant input#region").val(r.region);
		$("div#dialog_window_applicant input#city").val(r.city);
		$("div#dialog_window_applicant input#index").val(r.index);
		$("div#dialog_window_applicant input#address").val(r.full);
		$("div#dialog_window_applicant input#tel").val(null);
		if(data.fio.split(" ").length != 1){
			$("div#dialog_window_applicant input#surname").val(data.fio.split(" ")[0]);
			$("div#dialog_window_applicant input#name").val(data.fio.split(" ")[1]);
			$("div#dialog_window_applicant input#patronymic").val(data.fio.split(" ")[2]);
		}
	}
	else if(select_applicant.select2('data')[0] == undefined && applicant_last != undefined)
	$("div#dialog_window_applicant input").val("");
}
prepare_select_applicant();

var debtor_form_sel = "div#dialog_window_debtor";

var debtor_data_to_form= function(debtor_data){
	var sel = debtor_form_sel;

	var debtor = debtor_data.Должник;
	$(sel + " #name_org").text(debtor.Наименование);
	$(sel + " #inn").val(debtor.ИНН);
	$(sel + " #ogrn").val(debtor.ОГРН);
	$(sel + " #rregion").val(debtor.Регион);
	$(sel + " #raddress").val(debtor.Адрес);

	var manager= debtor_data.АУ;
	$(sel + " #surname").val(manager.Фамилия);
	$(sel + " #name").val(manager.Имя);
	$(sel + " #patronymic").val(manager.Отчество);

	var lact= debtor_data.Судебный_акт;
	$(sel + " #number").val(lact.Номер_дела);
	$(sel + " #date").val(lact.Дата);
	$(sel + " #court_ruling").val(lact.Суд);
}

var debtor_data_from_select2 = function(){
	var selected = select_debtor.select2('data');

	var selected_item = selected[0];

	var sdebtor = selected_item.data.Debtor;
	var debtor = {
		Наименование: sdebtor["!FullName"] ? sdebtor["!FullName"] : sdebtor["!ShortName"] ? sdebtor["!ShortName"] : selected_item.text
		,ИНН: safe_value(sdebtor,"!INN")
		,ОГРН: safe_value(sdebtor,"!OGRN")
		,Регион: safe_value(sdebtor,"!Region")
		,Адрес: safe_value(sdebtor,"!LegalAddress")
	};

	var smanager = selected_item.data.Manager;
	var manager = {
		Фамилия: safe_value(smanager,"!LastName")
		,Имя: safe_value(smanager,"!FirstName")
		,Отчество: safe_value(smanager,"!MiddleName")
	};

	var Судебный_акт = {};

	var llist = sdebtor.LegalCaseList.LegalCaseInfo;
	for (var i = 0; i < llist.length; i++)	{
		var lact = llist[i];
		Судебный_акт.Номер_дела = lact.Number;
		Судебный_акт.Суд = lact.Court;
		break;
	}

	return { Должник: debtor, АУ: manager, Судебный_акт: Судебный_акт };
}

var debtor_data_from_form = function(){
	var sel  = debtor_form_sel;

	var debtor = {
		Наименование: $(sel + " #name_org").text()
		,ИНН: $(sel + " #inn").val()
		,ОГРН: $(sel + " #ogrn").val()
		,Регион: $(sel + " #rregion").val()
		,Адрес: $(sel + " #raddress").val()
	};

	var manager= {
		Фамилия: $(sel + " #surname").val()
		,Имя: $(sel + " #name").val()
		,Отчество: $(sel + " #patronymic").val()
	}

	var Судебный_акт = {
		Номер_дела: $(sel + " #number").val()
		, Дата: $(sel + " #date").val()
		, Суд: $(sel + " #court_ruling").val()
	};

	return { Должник: debtor, АУ: manager, Судебный_акт: Судебный_акт };
}

const on_select_debtor = function(){
	console.log('on_select_debtor');
	var debtor_data = debtor_data_from_select2();
	debtor_data_to_form(debtor_data);
	OpenDebtorForm();
}
prepare_select_debtor();

applicant_last = undefined;
button_applicant.click(function(){
	applicant_last = select_applicant.select2('data')[0];
	applicant_is_opened = true;
	DW_applicant.dialog({width: 900, 
		dialogClass: 'no-close'});
});

$.get( "tpl/debtor.html", function(debtor_form_html) {
	$('#dialog_window_debtor').html(debtor_form_html);
	$('#dialog_window_debtor #date').datepicker();
});

var OpenDebtorForm = function(){
	DW_debtor.dialog({ 
		width: 900, height: 500
		, modal: true
		, buttons: {
			"OK": function() { DW_debtor.dialog("close"); }
			,"Отмена": function() { DW_debtor.dialog("close"); }
		}
	});
}

button_debtor.click(OpenDebtorForm);

$.datepicker.setDefaults($.datepicker.regional["ru"]);
$("input#date2").datepicker();
$("input#date1").datepicker();

date2[0].value = new Date().getDate().toString() + '.' + (new Date().getMonth() + 1).toString() + '.' + (new Date().getYear() + 1900).toString();
applicant_save.click(function(){
		DW_applicant.dialog('close');
});
  
debtor_save.click(function(){
	DW_debtor.dialog('close');
});

debtor_cancel.click(function(){
	inputs = $("div#dialog_window_debtor input");
		for(i = 0; i < inputs.length; i++)
			inputs[i].value = "";
		select_debtor.select2('destroy');
		prepare_select_debtor();
		DW_debtor.dialog('close');
		debtor_is_opened = false;
});

applicant_cancel.click(function(){
		applicant_last = undefined;
		inputs = $("div#dialog_window_applicant input");
		for(i = 0; i < inputs.length; i++)
			inputs[i].value = "";
		select_applicant.select2('destroy');
		prepare_select_applicant();
		DW_applicant.dialog('close');
		applicant_is_opened = false;
});

$("button#file_close").click(function(){
	$("div#dialog_window_add_file").dialog('close');
});
const add_button = $("button#add");
const input_file = $("input#file")[0];
const input_description = $("input#description")[0];
const files = [];
const tbody = $("tbody#add")[0];
files.erase = function(index){
	this.splice(index, 1);
	return this;
}
files.end = function(){
	return this[this.length - 1];
}
files.begin = function(){
	return this[0];
}
files.push = function(data){
	files[files.length] = data;
	tbody.innerHTML += `<tr id = \"tr` + (files.length - 1) + `\" title="Имя: ` + files.end().file.name + `\nРазмер: ` + files.end().file.size + `\nТип: ` + files.end().file.type + `">
			<td style="width: 30px; vertical-align: top;">` + (files.length) + `.</td><td style="display: inline-block; width: 650px;">` + files.end().text + `</td>
			<td style="text-align: right;">\
			<img src="images/cross.jpg" style="width: 20px; height: 20px;" title="Удалить" onclick="files.erase(' + (files.length - 1) + '); displaying_files(); "></td>
		</tr>`;
}

add_button.click(function() {
	if(file_name.innerText == '')	{
		alert("Нет файла для добавления!");
		return;
	}
	if(input_description.value == '')	{
		alert("Нет наименования!");
		return;
	}
	files.push({file: input_file.files[0], text: input_description.value});
	$("tbody#add tr#tr" + (files.length - 1))[0].innerHTML += "<input type=\"file\" hidden>";
	$("tbody#add tr#tr" + (files.length - 1) + " input")[0].files = input_file.files;
	input_description.value = '';
	file_name.innerHTML = '';
	input_file.value = '';
	displaying_files();
});

const isTrueDate = function(date){
	try	{
		const ar = date.split('.');
		if(ar.length != 3)
			return false;
		if(!(Number(ar[1]) >= 1 && Number(ar[1]) <= 12) || ar[1].length > 2)
			return false;
		if(!(Number(ar[0]) >= 1 && Number(ar[0]) <= 31) || ar[0].length > 2)
			return false;
		if(!(Number(ar[2]) >= 1980 && Number(ar[2]) <= 2050) || ar[2].length > 4)
			return false;
		return true;
	}
	catch(SyntaxError)	{
		return false;
	}
}

const isAdd = function(){
	$("td#file_name")[0].innerHTML = input_file.files.length ? input_file.files[0].name : "";
}

const isFull = function(){
	if($("div#dialog_window_applicant #name_org")[0].value == '')	{
		applicant_name_org = $("div#dialog_window_applicant #name_org")[0];
		DW_applicant.dialog({width: 900, 
		dialogClass: 'no-close'});
		applicant_name_org.style.border = "1px solid RED";
		applicant_name_org.onchange = function(){applicant_name_org.style.border = "";};
		return false;
	}
	if($("div#dialog_window_debtor #name_org")[0].value == '')	{
		debtor_name_org = $("div#dialog_window_debtor #name_org")[0];
		DW_debtor.dialog({ 
			width: 900, height: 500
			, modal: true
			, buttons: {
				"OK": function() { DW_debtor.dialog("close"); }
				,"Отмена": function() { DW_debtor.dialog("close"); }
			}
		});
		debtor_name_org.style.border = "1px solid RED";
		debtor_name_org.onchange = function(){debtor_name_org.style.border = "";};
		return false;
	}
	if($("input#debt_rub")[0].value == '' || $("input#debt_pen")[0].value == '')	{
		alert("Не заполнено поле основного долга!")
		return false;
	}
	if($("input#definition_number")[0].value == '')	{	
		alert("Не заполнено поле номера определения!");
		return false;
	}
	if(!isTrueDate($("#dialog_window_debtor input#date")[0].value))	{
		debtor_date = $("div#dialog_window_debtor #date")[0];
		DW_debtor.dialog({ 
			width: 900, height: 500
			, modal: true
			, buttons: {
				"OK": function() { DW_debtor.dialog("close"); }
				,"Отмена": function() { DW_debtor.dialog("close"); }
			}
		});
		debtor_date.style.border = "1px solid RED";
		debtor_date.onchange = function(){debtor_date.style.border = "";};
	}
	if(!isTrueDate($("input#date2")[0].value))	{
		alert("Не заполнена дата заполнения!");
		return false;
	}
	if(files.length == 0)	{
		alert("Не загружены документы!");
		return false;
	}
	if(select_name_org.select2('data').length == 0)	{
		alert("Не выбрана органиция, вынесшая вердикт");
		return false;
	}
	return true;
}

$("button.custom-file-upload").click(function(){
	$("div#dialog_window_add_file").dialog({
		width: 550,
		dialogClass: 'no-close'
	});
});

const displaying_files = function(){
	for(i = 0; i < files.length; i++){
		$("tbody#add tr")[i].title = "Имя: '" + files[i].file.name + '\nРазмер: ' + files[i].file.size + '\nТип: ' + files[i].file.type;
		$("tbody#add tr#tr" + i + " td")[1].innerHTML = files[i].text;
	}
	if(files.length < $("tbody#add tr").length)
		$("tbody#add tr")[files.length].outerHTML = '';
}
data = null;

var textareas_HTML = [];

const load_all_textarea = function(){
	ieditor = $("div.ieditor");
	for(i = 0; i < ieditor.length; i++){
		InlineEditor
		.create(ieditor[i])
		.catch(error=>{
		console.error( error );
		});
	}
}

load_all_textarea();

$("button#save").click(function(){
	if(isFull()){
		$("button[name=send]").click();
	}
});