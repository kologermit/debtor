﻿<!DOCTYPE HTML>
<html lang="ru">
 	<head>
		<meta charset="utf-8">
		<title>Заявление о включении в реестр требований кредиторов</title>

		<script type="text/javascript" src="js/vendors/jquery-3.4.1.js"></script>

		<script type="text/javascript" src="js/vendors/jquery.ui/jquery-ui.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2.full.js"></script>
		<script type="text/javascript" src="js/vendors/datepicker-ru.js"></script> 

		<link rel="stylesheet" type="text/css" href="js/vendors/select2/select2.css">
		<link rel="stylesheet" type="text/css" href="js/vendors/jquery.ui/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="style.css">

		<script type="text/javascript" src="js/vendors/editors/inline.js"></script>
	</head>

	<body bgcolor='white'>
		<form method="POST" name="main" action="create">
		<div id="dialog_window_applicant" hidden title="Информация о заявителе(кредиторе)">
			<table border="0">
				<tbody>
					<tr><td>Наименование</td><td><input id='name_org' name="applicant_name_org"></td></tr>
					<tr><td colspan="2"><div class="common"><b>Регистрационные данные</b></div></td></tr>
					<tr><td>ИНН</td><td><input id='inn' name="applicant_inn"></td></tr>
					<tr><td>ОГРН</td><td><input id='ogrn' name="applicant_ogrn"></td></tr>
					<tr><td colspan="2"><div class="common"><b>Юридический адрес</b></div></td></tr>
					<tr><td>Регион</td><td><input id='uregion' name="applicant_uregion"></td></tr>
					<tr><td>Город</td><td><input id='ucity' name="applicant_ucity"></td></tr>
					<tr><td>Почтовый индекс</td><td><input id='uindex' name="applicant_uindex"></td></tr>
					<tr><td>Адрес</td><td><input id='uaddress' name="applicant_uaddress"></td></tr>
					<tr><td colspan="2"><div class="common"><b>Адрес почтовых уведомлений</b></div></td></tr>
					<tr><td>Регион</td><td><input id='region' name="applicant_region"></td></tr>
					<tr><td>Город</td><td><input id='city' name="applicant_city"></td></tr>
					<tr><td>Почтовый индекс</td><td><input id='index' name="applicant_index"></td></tr>
					<tr><td>Адрес</td><td><input id='address' name="applicant_address"></td></tr>
					<tr><td>Телефон/факс</td><td><input id='tel' name="applicant_tel"></td></tr>
					<tr>
						<td colspan="2">
							<div class="common"><b>Ф.И.О. руководителя кредитора - <br>юридического лица</b></div>
						</td>
					</tr>
					<tr><td>Фамилия</td><td><input id='surname' name="applicant_surname"></td></tr>
					<tr><td>Имя</td><td><input id='name' name="applicant_name"></td></tr>
					<tr><td>Отчество</td><td><input id='patronymic' name="applicant_patronymic"></td></tr>
					<tr>
						<td><center><button form="main" id='save'>Ок</button></center></td>
						<td><center><button form="main" id="cancel">Отменить</button></center></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div id="dialog_window_debtor" hidden title="Информация о должнике, арбитражном управляющем и процедуре"></div>

		<div id="dialog_window_add_file" hidden title="">
			<table>
				<thead>
					<tr>
						<td>Наименование</td>
						<td> <input type="text" id='description' name="description" style="width: 400px;"></td>
					</tr>
					<tr>
						<td><label><img src="images/file.jpg"><input type="file" id="file" hidden onchange="isAdd()"></label></td>
						<td><button form="main" id='add'>Добавить</button></td>
					</tr>
					<tr><td id="file_name" colspan="2"></td></tr>
				</thead>
			</table>
			<button form="main" id="file_close">Закрыть</button>
		</div>

		<center>
			<div class="headline">
				<small>Заявление</small><br/>
				о включении в реестр требований кредиторов
			</div>

			<table>
				<tbody>
					<tr>
						<td>Заявитель</td>
						<td>
							<select id="applicant" lang="ru" tags></select>
							<button form="main" id="applicant" title="Нажмите, чтобы проверить или
изменить информацию о заявителе" 
									style="width: 70px;">детали..</button>
						</td>
						<td>
							<img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
									title="Выберите имя заявителя из списка, и проверьте
информацию в небольшом далоговом окне">
						</td>
					</tr>
					<tr>
						<td>Должник</td>
						<td>
							<select id="debtor" lang="ru"></select>
							<button form="main" id="debtor" title="Нажмите, чтобы проверить или
изменить информацию о долднике" style="width: 70px;">детали..</button>
						</td>
						<td>
							<img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
									title="Выберите имя должника из списка, и проверьте 
информацию в небольшом далоговом окне">
						</td>
					</tr>
					<tr>
						<td colspan="3">например: 1833058504</td>
					</tr>
				</tbody>
			</table>

			<table>
				<tbody>
					<tr>
						<td colspan="4"><div class="common"><b>Размер требований</b></div></td>
					</tr>
					<tr>
						<td style="text-align: right;">осовной долг</td>
						<td>
							<input type="number" id="debt_rub" 
								title="Введите количество рублей" name="debt_rub"> руб., 
						</td>
						<td colspan="2">пени
							<input type="number" name="1" id="debt_pen" 
								title="Введите количество пени" name="debt_pen"> руб.
						</td>
						<td><img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
								title="Введите основной долг в рублях и пени"></td>
					</tr>
					<tr>
						<td>Подтверждается определением №</td>
						<td>
							<input type="text" title="Введите номер определения" 
								id='definition_number' name="definition_number">
						</td>
						<td>От</td>
						<td>
							<input type="text" title="Введите дату (в форме 'дд/мм/гггг')" 
									id='date1' name="date1">
						</td>
						<td>
							<img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
									title="Введите дату (в форме 'дд/мм/гггг')">
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">Вынесеным</td>
						<td colspan="3">
							<select id="name_org"></select>
						</td>
						<td>
							<img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
								title="Выберите организациюиз списка,
которая вынесла вердикт">
						</td>
					</tr>
				</tbody>
			</table>

			<table>
				<tbody>
					<tr>
						<td colspan="2">
							<div class="common"><b>Пояснительная записка</b></div>
						</td>
					</tr>
					<tr>
						<td id='more_information' colspan="2" style="width: 730px;">
							<div class="ieditor" style=" border: 4px double black; width: 720px;"></div>
						</td>
						<td>
							<img src="images/question_mark.jpg" style="width: 20px; height: 20px" 
								title="Введите дополнительную информацию">
						</td>
					</tr>
					<tr>
						<td>Прилагаемые документы</td>
						<td style="text-align: right;"><button form="main" class="custom-file-upload">Приложить</button></td>
					</tr>
					<tr>
						<td colspan="2">
							<table id="add" class="a">
								<tbody id="add"></tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							Дата заполнения
						</td>
						<td>
							<input type="text" id='date2' name="date2" 
								title="Введите дату заполнения требования
(дата ставится автоматически на сегодня)">
						</td>
					</tr>
				</tbody>
			</table>

			<table>
				<tbody>
					<tr>
						<td><button form="main" form="main" id="send">Отправить</button></td>
					</tr>
				</tbody>
			</table>
		</center>
		</form>
		<script type="text/javascript" src="js/script.js"></script>
	</body>
</html>