<?

function mysqli_stmt_get_result_without_mysqlnd( $Statement )
{
	$RESULT = array();
	$Statement->store_result();
	for ( $i = 0; $i < $Statement->num_rows; $i++ )
	{
		$Metadata = $Statement->result_metadata();
		$PARAMS = array();
		while ( $Field = $Metadata->fetch_field() )
		{
			$PARAMS[] = &$RESULT[ $i ][ $Field->name ];
		}
		call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
		$Statement->fetch();
		$RESULT[$i]= (object)$RESULT[$i];
	}
	return $RESULT;
}

class Reusable_mysql_connection
{
	private $db_link= null;

	function get_db_link()
	{
		return $this->db_link;
	}

	function __construct($options= null)
	{
		global $default_db_options;

		if (!isset($options) || null==$options)
			$options= $default_db_options;

		$db_link = mysqli_connect($options->host, $options->user, $options->password, $options->dbname);

		if (!$db_link)
			die('Ошибка соединения: ' . mysql_error());

		if (!mysqli_set_charset($db_link, $options->charset))
			die('Ошибка соединения при установке кодировки: ' . mysql_error());

		$this->db_link= $db_link;
	}

	function __destruct()
	{
		if (null!=$this->db_link)
		{
			mysqli_close($this->db_link);
			$this->db_link= null;
			// write_to_log('mysqli_close ..');
		}
	}

	function execute_query_internal($txt_query,$parameters,$return_result)
	{
		$stmt= mysqli_stmt_init($this->db_link);

		$query_result= null;
		if (!mysqli_stmt_prepare($stmt, $txt_query))
		{
			write_to_log('mysqli_error: '.mysqli_error($this->db_link));
			write_to_log('can not prepare query: '.$txt_query);
			throw new Exception('can not prepare query!');
		}
		else
		{
			prepare_stmt_parameters($parameters,$stmt);
			if (!mysqli_stmt_execute($stmt))
			{
				$error= 'mysqli_stmt_error: '.mysqli_stmt_error($stmt);
				write_to_log($error);
				write_to_log('can not execute query: '.$txt_query);
				throw new Exception("can not execute query! ($error)");
			}
			if (true===$return_result)
			{
				$query_result= get_query_result_rows($stmt);
			}
			else if (false==$return_result)
			{
			}
			else if (3==$return_result)
			{
				$query_result= mysqli_insert_id($this->db_link);
			}
			else if (4==$return_result)
			{
				$rows= get_query_result_rows($stmt);
				$found_rows_result = mysqli_query($this->db_link, "select found_rows();");
				$found_rows_result_row= $found_rows_result->fetch_assoc();
				$num_rows = $found_rows_result_row['found_rows()'];
				return array('rows'=>$rows,'found_rows'=>$num_rows);
			}
			else if (isset($return_result))
			{
				write_to_log($return_result);
			}
			else
			{
				write_to_log('!isset(return_result)');
			}
		}

		if (false!=$return_result)
			return $query_result;
	}

	function execute_query($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,true);
	}

	function execute_query_get_last_insert_id($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,3);
	}

	function execute_query_get_found_rows($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,4);
	}

	function execute_query_no_result($txt_query,$parameters= array())
	{
		$this->execute_query_internal($txt_query,$parameters,false);
	}

	function begin_transaction()
	{
		mysqli_query($this->db_link, "START TRANSACTION");
	}

	function commit()
	{
		mysqli_query($this->db_link, "COMMIT");
	}

	function rollback()
	{
		mysqli_query($this->db_link, "ROLLBACK");
	}
}

global $default_mysql_connection;
$default_mysql_connection= null;

function default_dbconnect()
{
	global $default_mysql_connection;

	if (null==$default_mysql_connection)
		$default_mysql_connection= new Reusable_mysql_connection();

	return $default_mysql_connection;
}

function prepare_stmt_parameters($p,$stmt)
{
	$parameters_len= count($p);
	if ($parameters_len>1)
	{
		$blobs= array();
		$types= $p[0];
		for ($i= 0; $i<$parameters_len-1; $i++)
		{
			if ('b'==substr($types,$i,1))
			{
				$blobs[$i]= $p[$i+1];
				$p[$i+1]= null;
			}
		}
		switch ($parameters_len)
		{
			case 0: break;
			case 1: break;
			case 2: mysqli_stmt_bind_param($stmt, $p[0], $p[1]); break;
			case 3: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2]); break;
			case 4: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3]); break;
			case 5: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4]); break;
			case 6: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5]); break;
			case 7: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6]); break;
			case 8: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7]); break;
			case 9: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8]); break;
			case 10: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]); break;
			case 11: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10]); break;
			case 12: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11]); break;
			case 13: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12]); break;
			case 14: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13]); break;
			case 15: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14]); break;
			case 16: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15]); break;
			case 17: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]); break;
			case 18: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17]); break;
			case 19: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18]); break;
			case 20: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19]); break;
			case 21: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20]); break;

			case 22: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21]); break;
			case 23: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22]); break;
			case 24: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23]); break;
			case 25: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24]); break;
			case 26: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24], $p[25]); break;
			case 27: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24], $p[25], $p[26]); break;
			case 28: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24], $p[25], $p[26], $p[27]); break;
			case 29: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24], $p[25], $p[26], $p[27], $p[28]); break;
			case 30: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20], $p[21], $p[22], $p[23], $p[24], $p[25], $p[26], $p[27], $p[28], $p[29]); break;
		}
		foreach ($blobs as $i => $blob)
		{
			mysqli_stmt_send_long_data($stmt,$i,$blob);
		}
	}
}

function get_query_result_rows($stmt)
{
	$query_result= array();
	$result = mysqli_stmt_get_result_without_mysqlnd($stmt);
	while ($row = array_shift($result))
	{
		$query_result[]= $row;
	}
	return $query_result;
}

function execute_query_internal($txt_query,$parameters,$return_result)
{
	default_dbconnect()->execute_query_internal($txt_query,$parameters,$return_result);
}

function execute_query($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,true);
}

function execute_query_get_last_insert_id($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,3);
}

function execute_query_get_found_rows($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,4);
}

function execute_query_no_result($txt_query,$parameters= array())
{
	default_dbconnect()->execute_query_internal($txt_query,$parameters,false);
}

function std_filter_rule_builder_dt_compare($l,$rule)
{
	$field= mysqli_real_escape_string($l,$rule->field);
	$txt= trim($rule->data);
	$txt_len= strlen($txt);
	if (2>$txt_len)
	{
		return "";
	}
	else
	{
		$c= substr($txt,0,1);
		switch ($c)
		{
			case '>':
			case '<':
				$tail= mysqli_real_escape_string($l,trim(substr($txt,1,$txt_len-1)));
				$res= " and $field $c '$tail' ";
				return $res;
			default: 
				$txt= mysqli_real_escape_string($l,$txt);
				return " and $field = '$txt' ";
		}
	}
	
}

function std_filter_rule_builder($l,$rule)
{
	$field= $rule->field;
	$where= " and $field";
	switch ($rule->op) // В будущем будет больше вариантов для всех вохможных условий jqGrid
	{
		case 'eq': $where .= " = '".mysqli_real_escape_string($l,$rule->data)."'"; break;
		case 'ne': $where .= " != '".mysqli_real_escape_string($l,$rule->data)."'"; break;
		case 'bw': $where .= " LIKE '".mysqli_real_escape_string($l,$rule->data)."%'"; break;
		case 'bn': $where .= " NOT LIKE '".mysqli_real_escape_string($l,$rule->data)."%'"; break;
		case 'ew': $where .= " LIKE '%".mysqli_real_escape_string($l,$rule->data)."'"; break;
		case 'en': $where .= " NOT LIKE '%".mysqli_real_escape_string($l,$rule->data)."'"; break;
		case 'cn': $where .= " LIKE '%".mysqli_real_escape_string($l,$rule->data)."%'"; break;
		case 'nc': $where .= " NOT LIKE '%".mysqli_real_escape_string($l,$rule->data)."%'"; break;
		case 'nu': $where .= " IS NULL"; break;
		case 'nn': $where .= " IS NOT NULL"; break;
		case 'in': $where .= " IN ('".str_replace(",", "','", mysqli_real_escape_string($l,$rule->data))."')"; break;
		case 'ni': $where .= " NOT IN ('".str_replace(",", "','", mysqli_real_escape_string($l,$rule->data))."')"; break;
	}
	return $where;
}

function execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order='')
{
	$con= default_dbconnect();
	$l= $con->get_db_link();
	$where= '';
	if (isset($_GET['filters']))
	{
		$filters = $_GET['filters'];
		$filters= str_replace("\\\"", "\"", $filters);
		$filters = json_decode($filters);
		if (isset($filters->rules))
		{
			foreach ($filters->rules as $index => $rule)
			{
				$field= $rule->field;
				if (is_callable($filter_rule_builders[$field]))
				{
					$func= $filter_rule_builders[$field];
					$where.= $func($l,$rule);
				}
			}
		}
		
	}
	$page= 0;
	$limit_position= 0;
	$limit_size= 10;
	if (isset($_GET['rows']))
	{
		$limit_size= $_GET['rows'];
		if (isset($_GET['page']))
		{
			$page= $_GET['page'];
			$limit_position= mysqli_real_escape_string($l,($page-1)*$limit_size);
		}
		$limit_size= mysqli_real_escape_string($l,$limit_size);
	}

	$txt_query= "select $fields $from_where $where $order limit $limit_position, $limit_size;";
	$rows= execute_query($txt_query,array());

	$txt_query= "select count(*) count $from_where $where";
	$rows_count= execute_query($txt_query,array());
	$rows_count= $rows_count[0]->count;

	return array(
		'page'=>$page,
		'total'=>ceil($rows_count/$limit_size),
		'records'=>$rows_count,
		'rows'=>$rows
	);
}

function execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$order='')
{
	$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order);
	echo json_encode($result);
}

class Base_crud
{
	private function ThrowEmptyImplementation($method_name)
	{
		$class_name= get_class($this);
		throw new Exception("Empty implementation for method $class_name::$method_name!");
	}
	function create($obj) { $this->ThrowEmptyImplementation('create'); }
	function read($id) { $this->ThrowEmptyImplementation('read'); }
	function update($id,$obj) { $this->ThrowEmptyImplementation('update'); }
	function delete($ids) { $this->ThrowEmptyImplementation('delete'); }

	function process_cmd()
	{
		switch ($_GET['cmd'])
		{
			case 'add': $this->create($_POST); break;
			case 'get': $this->read($_GET['id']); break;
			case 'update': $this->update($_GET['id'],$_POST); break;
			case 'delete': $this->delete(explode(',',$_GET['id'])); break;
		}
	}
}
?>