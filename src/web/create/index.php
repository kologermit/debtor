<?
	include "../../assets/config.php";
	include "log.php";
	include "db.php";
	// execute_query("INSERT INTO `data` (`applicant`, `debtor`, `debt_rub`, `debt_pen`, `definition_number`, `date1`, `more_information`, `date2`, `name_org`, `files`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", ["ssddssssss", 
	// 	$_GET["applicant"], 
	// 	$_GET["debtor"], 
	// 	$_GET["debt_rub"], 
	// 	$_GET["debt_pen"], 
	// 	$_GET["definition_number"], 
	// 	$_GET["date1"], 
	// 	$_GET["more_information"], 
	// 	$_GET["date2"],
	// 	$_GET["name_org"],
	// 	$_GET["files"]]);
	// $_GET["applicant"] = json_decode($_GET["applicant"]);
	// $_GET["debtor"] = json_decode($_GET["debtor"]);
	function num2str($num) {
		$nul = 'ноль';
		$ten = array(
			array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
			array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
		);
		$a20 = array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
		$tens = array(2 =>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
		$hundred = array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
		$unit = array( // Units
			array('копейка' ,'копейки' ,'копеек',	 1),
			array('рубль'   ,'рубля'   ,'рублей'    ,0),
			array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
			array('миллион' ,'миллиона','миллионов' ,0),
			array('миллиард','милиарда','миллиардов',0),
		);
		//
		list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
		$out = array();
		if (intval($rub) > 0) {
			foreach(str_split($rub,3) as $uk => $v) { 
				if (!intval($v)) continue;
				$uk = sizeof($unit) - $uk - 1; 
				$gender = $unit[$uk][3];
				list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
				$out[] = $hundred[$i1];
				if ($i2 > 1) $out[] = $tens[$i2].' '.$ten[$gender][$i3]; 
				else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; 
				if ($uk > 1) $out[] = morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
			} 
		}
		else $out[] = $nul;
		$out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); 
		$out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); 
		return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
	}
	function morph($n, $f1, $f2, $f5) {
		$n = abs(intval($n)) % 100;
		if ($n > 10 && $n < 20) return $f5;
		$n = $n % 10;
		if ($n > 1 && $n < 5) return $f2;
		if ($n == 1) return $f1;
		return $f5;
	}
var_dump($_FILES);
?>
<div style = "text-align: right;">
<div class="ieditor" style="width: 100%; height: 40%; text-align: right;">
В <?=str_replace("судом", "суд", str_replace("ым", "ый", $_GET["name_org"]))?>
<br>Заявитель: <?=$_GET["applicant_name_org"]?>
<br>Адрес: <?=$_GET["applicant_address"]?> 
<br>ИНН: <?=$_GET["applicant_inn"]?>, 
<br>ОГРН: <?=$_GET["applicant_ogrn"]?>.
<br>Должник: <?=$_GET["debtor_name_org"]?>
<br>Адрес: <?=$_GET["debtor_raddress"]?>
<br>ИНН: <?=$_GET["debtor_inn"]?>, 
<br>ОГРН: <?=$_GET["debtor_ogrn"]?>
<br>Дело №: <?=$_GET["debtor_court_ruuling"]?></div></div>
<center><strong><br>Заявление должника о включении в реестр требованией кредиторов должника</strong></center>
<div class="ieditor" style="width: 100%; height: 60%;">
У должника <?=$_GET["applicant_name_org"]?> имеется задолжность перед <?=$_GET["debtor_name_org"]?>,которое возникло на основании неисполнения обязательств по договору купли-продажи за <?=$_GET["debtor"]->number?> от <?=$_GET["debtor"]->date?>. Размер задолжности составляет по данному договору составляет <?=$_GET["debt_rub"]?> руб. (<?=num2str($_GET["debt_rub"])?>), пени: <?=$_GET["debt_pen"]?> руб. (<?=num2str($_GET["debt_pen"])?>)
<br>Определением <?=str_replace("судом", "суда", str_replace("ым", "ого", $_GET["name_org"]))?> от <?=$_GET["debtor"]->date?> в отношении должника <?=$_GET["debtor_name_org"]?> введена процедура наблюдения.
<br><?=str_replace("<br>", "\n", $_GET["more_information"] ? $_GET["more_information"] : "")?>
<br>На основании изложеных фактов и подтверждается определением № <?=$_GET["definition_number"]?> от <?=$_GET["date1"];?>
<br>ПРОШУ
<br>признать обоснованными и подлежащими включению в реестр требований кредиторов <?=$_GET["applicant_name_org"]?> требования 
<?=$_GET["debtor_name_org"]?> в составе требований кредиторов в размере <?=$_GET["debt_rub"]?> руб. (<?=num2str($_GET["debt_rub"])?>), пени: <?=$_GET["debt_pen"]?> руб. (<?=num2str($_GET["debt_pen"])?>).
<br>Приложенные документы: 
<br><? 
	if(isset($_GET["file_names"]))
	$_GET["file_names"] = json_decode($_GET["file_names"]);
	else
	$_GET["file_names"] = json_decode('[{"id": 0, "text": "13123"}, {"id": 1, "text": "497823789"}]');
	$size = count($_GET["file_names"]);
	for($i = 0; $i < $size; $i++)
		printf($_GET["file_names"][$i]->id.". ".$_GET["file_names"][$i]->text."<br>");
?>
Директор <?=$_GET["applicant_name_org"]?> <?=$_GET["applicant"]->surname?> <?=$_GET["applicant"]->name?> <?=$_GET["applicant"]->patronymic?> <?=$_GET["date2"]?></div>
<p><input type="email" id="email" placeholder="Эл. Почта">
<p><button onclick="Send($('input#email')[0].value)" id="send">SEND</button>